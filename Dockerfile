FROM node:16-alpine
WORKDIR /app/library
COPY . .
RUN npm install --production
CMD ["npm", "start"]