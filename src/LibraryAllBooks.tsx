import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { useNavigate, useParams } from "react-router-dom";
import {
  fetchNumberOfAllBooks,
  fetchBookPaging,
  fetchSearchedBooks,
  setSearchedBook,
  setSelectedList,
} from "./features/booksSlice";
import { fetchBook, setBookId, setOpenModal } from "./features/modalSlice";
import { setPageAndBooksPerPage } from "./features/pageConfigSlice";
import BooksList from "./components/booksList/booksList";
import SearchForm from "./components/searchForm/searchForm";
import BooksPerPage from "./components/paging/booksPerPage";
import BookModal from "./components/modal/modal";
import Loading from "./components/loading/loading";
import Paging from "./components/paging/paging";
import ListSelection from "./components/listSelection/listSelection";
import "./Library.css";

function LibraryAllBooks() {
  const { search, per_page, page, id } = useParams();
  const currentPage = useAppSelector((state) => state.pageConfig.currentPage);
  const booksPerPage = useAppSelector((state) => state.pageConfig.booksPerPage);
  const openModal = useAppSelector((state) => state.modal.openModal);
  const modalLoading = useAppSelector((state) => state.modal.loading);
  const currentPageFav = useAppSelector(
    (state) => state.pageConfig.currentPageFav
  );

  const searchedBookState = useAppSelector(
    (state) => state.books.searchedBook
  ) as string;

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchNumberOfAllBooks());
    dispatch(setSelectedList({ list: "all" }));
    dispatch(setSearchedBook({ searchedBook: "" }));
  }, [dispatch]);

  useEffect(() => {
    const pageNumber: number = Number(page);
    const perPageNumber: number = Number(per_page);

    if (!!pageNumber && !!perPageNumber) {
      dispatch(
        setPageAndBooksPerPage({
          page: pageNumber,
          booksPerPage: perPageNumber,
        })
      );

      dispatch(
        fetchBookPaging({
          currentPage: pageNumber,
          booksPerPage: perPageNumber,
        })
      );
    }
  }, [page, per_page, dispatch]);

  useEffect(() => {
    const bookIdNumber: number = Number(id);

    if (!!bookIdNumber) {
      dispatch(setBookId({ bookId: bookIdNumber }));
      dispatch(setOpenModal({ openModal: true }));
    }
  }, [id, dispatch]);

  useEffect(() => {
    if (!!search) {
      dispatch(setSearchedBook({ searchedBook: search }));
      dispatch(fetchSearchedBooks(search));
    } else if (!!id) {
      dispatch(fetchBook(Number(id)));
    } else {
      navigate(`/per_page/${booksPerPage}/page/${currentPage}`, {
        replace: true,
      });
    }
  }, [
    currentPage,
    booksPerPage,
    search,
    id,
    dispatch,
    navigate,
    currentPageFav,
  ]);

  useEffect(() => {
    if (searchedBookState) navigate(`/search/${searchedBookState}`);
    else if (!id)
      navigate(`/per_page/${booksPerPage}/page/${currentPage}`, {
        replace: true,
      });
  }, [searchedBookState, id]);

  function displayModal() {
    if (modalLoading === "pending") return <Loading />;
    if (modalLoading === "succeeded") return <BookModal />;
  }

  return (
    <div className="library__container">
      <SearchForm />
      <div className="library__selectContainer">
        <ListSelection />
        <BooksPerPage />
      </div>
      <BooksList />
      <Paging />
      {openModal && displayModal()}
    </div>
  );
}

export default LibraryAllBooks;
