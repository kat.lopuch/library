import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import booksSlice from "../features/booksSlice";
import pageConfigSlice from "../features/pageConfigSlice";
import modalSlice from "../features/modalSlice";

export const store = configureStore({
  reducer: {
    books: booksSlice,
    pageConfig: pageConfigSlice,
    modal: modalSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
