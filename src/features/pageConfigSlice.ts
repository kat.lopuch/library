import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface Page {
  currentPage: number;
  currentPageFav: number;
  booksPerPage: number;
}

const initialState: Page = {
  currentPage: 1,
  currentPageFav: 1,
  booksPerPage: 12,
};

const pageConfigSlice = createSlice({
  name: "pageConfig",
  initialState,
  reducers: {
    setCurrentPage: (state, action: PayloadAction<{ page: number }>) => {
      state.currentPage = action.payload.page;
    },
    setCurrentPageFav: (state, action: PayloadAction<{ page: number }>) => {
      state.currentPageFav = action.payload.page;
    },
    setBooksPerPage: (
      state,
      action: PayloadAction<{ booksPerPage: number }>
    ) => {
      state.booksPerPage = action.payload.booksPerPage;
    },
    setPageAndBooksPerPage: (
      state,
      action: PayloadAction<{ page: number; booksPerPage: number }>
    ) => {
      state.currentPage = action.payload.page;
      state.booksPerPage = action.payload.booksPerPage;
    },
  },
});

export const {
  setCurrentPage,
  setCurrentPageFav,
  setPageAndBooksPerPage,
  setBooksPerPage,
} = pageConfigSlice.actions;
export default pageConfigSlice.reducer;
