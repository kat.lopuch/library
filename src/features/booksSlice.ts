import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";

export interface Book {
  id: number;
  title: string;
  authors: string;
  description: string;
  rating: number;
  genre_list: string;
  image_url: string;
  Quote1: string;
  Quote2: string;
  Quote3: string;
}

export interface Configuration {
  currentPage: number;
  booksPerPage: number;
}

interface BooksState {
  nrOfAllBooks: number;
  books: Book[] | [];
  favBooks: Book[];
  selectedList: "all" | "favorites";
  searchedBook: string | undefined;
  error: string | null;
}

const initialState: BooksState = {
  books: [],
  favBooks: [],
  selectedList: "all",
  nrOfAllBooks: 0,
  searchedBook: "",
  error: null,
};

export const fetchNumberOfAllBooks = createAsyncThunk<{
  nrOfAllBooks: number;
}>("books/fetchAllBooks", async () => {
  const response = await fetch(`https://example-data.draftbit.com/books`);
  const responseJson = await response.json();
  return {
    nrOfAllBooks: responseJson.length as number,
  };
});

export const fetchBookPaging = createAsyncThunk<
  { books: Book[] },
  Configuration
>("books/fetchBookPaging", async (configuration: Configuration) => {
  const { booksPerPage, currentPage } = configuration;
  const response = await fetch(
    `https://example-data.draftbit.com/books?_page=${currentPage}&_limit=${booksPerPage}`
  );
  const responseJson = await response.json();
  return { books: responseJson as Book[] };
});

export const fetchSearchedBooks = createAsyncThunk<{ books: Book[] }, string>(
  "books/searchedBooks",
  async (searchedBook: string | undefined) => {
    const response = await fetch(
      `https://example-data.draftbit.com/books?q=${searchedBook}`
    );
    const responseJson = await response.json();
    return { books: responseJson as Book[] };
  }
);

const booksSlice = createSlice({
  name: "books",
  initialState,
  reducers: {
    setSearchedBook: (
      state,
      action: PayloadAction<{ searchedBook: string | undefined }>
    ) => {
      state.searchedBook = action.payload.searchedBook;
    },
    addFavBook: (state, action: PayloadAction<{ favBook: Book }>) => {
      state.favBooks.push(action.payload.favBook);
      localStorage.setItem("favorites", JSON.stringify(state.favBooks));
    },
    removeFavBook: (state, action: PayloadAction<{ favBookId: number }>) => {
      let index = state.favBooks.findIndex(
        (book) => book.id === action.payload.favBookId
      );
      state.favBooks.splice(index, 1);
      localStorage.setItem("favorites", JSON.stringify(state.favBooks));
    },
    searchFavBooks: (state) => {
      const searchPhrase = state.searchedBook as string;
      state.books = state.favBooks.filter(
        (book) =>
          book.title.toLowerCase().includes(searchPhrase) ||
          book.authors.toLowerCase().includes(searchPhrase) ||
          book.description.toLowerCase().includes(searchPhrase) ||
          book.genre_list.toLowerCase().includes(searchPhrase)
      );
    },
    setFavBooksFromLocalStorage: (state) => {
      let favorites = localStorage.getItem("favorites") as string;
      let favoriteBooks = JSON.parse(favorites) as Book[];

      if (favoriteBooks) state.favBooks = JSON.parse(favorites) as Book[];

      if (state.favBooks) state.nrOfAllBooks = state.favBooks.length;
    },
    setFavBooks: (
      state,
      action: PayloadAction<{ page: number; perPage: number }>
    ) => {
      state.books = state.favBooks.slice(
        (action.payload.page - 1) * action.payload.perPage,
        action.payload.page * action.payload.perPage
      );
    },
    setSelectedList: (
      state,
      action: PayloadAction<{ list: "all" | "favorites" }>
    ) => {
      state.selectedList = action.payload.list;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchNumberOfAllBooks.fulfilled, (state, action) => {
        state.nrOfAllBooks = action.payload.nrOfAllBooks;
      })
      .addCase(fetchBookPaging.fulfilled, (state, action) => {
        state.books = action.payload.books.filter(
          (book) =>
            !!book.id &&
            !!book.title &&
            !!book.authors &&
            !!book.description &&
            !!book.rating &&
            !!book.genre_list &&
            !!book.image_url &&
            !!book.Quote1 &&
            !!book.Quote2 &&
            !!book.Quote3
        );
      })
      .addCase(fetchSearchedBooks.fulfilled, (state, action) => {
        state.books = action.payload.books;
      });
  },
});

export const {
  setSearchedBook,
  addFavBook,
  removeFavBook,
  setSelectedList,
  setFavBooksFromLocalStorage,
  searchFavBooks,
  setFavBooks,
} = booksSlice.actions;
export default booksSlice.reducer;
