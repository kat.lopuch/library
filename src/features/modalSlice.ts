import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { Book } from "./booksSlice";

interface ModalState {
  book: Book;
  bookId: number;
  openModal: boolean;
  loading: "idle" | "pending" | "succeeded" | "failed";
}

const initialState: ModalState = {
  book: {
    id: 0,
    title: "",
    authors: "",
    description: "",
    rating: 0,
    genre_list: "",
    image_url: "",
    Quote1: "",
    Quote2: "",
    Quote3: "",
  },
  bookId: 0,
  openModal: false,
  loading: "idle",
};

export const fetchBook = createAsyncThunk<
  {
    book: Book;
  },
  number
>("books/book", async (bookId: number) => {
  const response = await fetch(
    `https://example-data.draftbit.com/books/${bookId}`
  );
  const responseJson = await response.json();
  return {
    book: responseJson as Book,
  };
});

const modalSlice = createSlice({
  name: "books",
  initialState,
  reducers: {
    setBookId: (state, action: PayloadAction<{ bookId: number }>) => {
      state.bookId = action.payload.bookId;
    },
    setOpenModal: (state, action: PayloadAction<{ openModal: boolean }>) => {
      state.openModal = action.payload.openModal;
    },
    clearModal: () => initialState,
  },
  extraReducers(builder) {
    builder
      .addCase(fetchBook.fulfilled, (state, action) => {
        state.book = action.payload.book;
        state.loading = "succeeded";
      })
      .addCase(fetchBook.pending, (state) => {
        state.loading = "pending";
      })
      .addCase(fetchBook.rejected, (state) => {
        state.loading = "failed";
      });
  },
});
export const { setBookId, setOpenModal, clearModal } = modalSlice.actions;
export default modalSlice.reducer;
