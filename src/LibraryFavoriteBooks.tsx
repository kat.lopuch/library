import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { useNavigate, useParams } from "react-router-dom";
import {
  setFavBooksFromLocalStorage,
  setSearchedBook,
  setSelectedList,
  searchFavBooks,
  setFavBooks,
} from "./features/booksSlice";
import { setBookId, setOpenModal, fetchBook } from "./features/modalSlice";
import { setCurrentPageFav, setBooksPerPage } from "./features/pageConfigSlice";
import BooksList from "./components/booksList/booksList";
import SearchForm from "./components/searchForm/searchForm";
import BooksPerPage from "./components/paging/booksPerPage";
import BookModal from "./components/modal/modal";
import Loading from "./components/loading/loading";
import Paging from "./components/paging/paging";
import ListSelection from "./components/listSelection/listSelection";
import BookAnimation from "./components/bookAnimation/bookAnimation";
import "./Library.css";

function LibraryFavoriteBooks() {
  const { search, per_page, page, id } = useParams();
  const favListLength = useAppSelector((state) => state.books.favBooks.length);
  const booksPerPage = useAppSelector((state) => state.pageConfig.booksPerPage);
  const openModal = useAppSelector((state) => state.modal.openModal);
  const modalLoading = useAppSelector((state) => state.modal.loading);
  const currentPageFav = useAppSelector(
    (state) => state.pageConfig.currentPageFav
  );

  const searchedBookState = useAppSelector(
    (state) => state.books.searchedBook
  ) as string;

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(setFavBooksFromLocalStorage());
    dispatch(setSelectedList({ list: "favorites" }));
    dispatch(setSearchedBook({ searchedBook: "" }));
  }, [dispatch]);

  useEffect(() => {
    const perPageNumber: number = Number(per_page);
    const pageNumber: number = Number(page);

    if (!!pageNumber && !!perPageNumber) {
      dispatch(
        setCurrentPageFav({
          page: pageNumber,
        })
      );

      dispatch(
        setBooksPerPage({
          booksPerPage: perPageNumber,
        })
      );

      dispatch(setFavBooks({ page: pageNumber, perPage: perPageNumber }));
    }
  }, [page, per_page, favListLength, dispatch]);

  useEffect(() => {
    if (!!search) {
      dispatch(setSearchedBook({ searchedBook: search }));
      dispatch(searchFavBooks());
      return;
    }

    if (!!id) {
      const bookIdNumber: number = Number(id);

      dispatch(setBookId({ bookId: bookIdNumber }));
      dispatch(setOpenModal({ openModal: true }));
      dispatch(fetchBook(bookIdNumber));
      return;
    }

    navigate(`/favBooks/per_page/${booksPerPage}/page/${currentPageFav}`);
  }, [booksPerPage, search, id, currentPageFav, dispatch, navigate]);

  useEffect(() => {
    if (searchedBookState) navigate(`/favBooks/search/${searchedBookState}`);
    else if (!id) navigate("/favBooks");
  }, [searchedBookState, id]);

  function displayModal() {
    if (modalLoading === "pending") return <Loading />;
    if (modalLoading === "succeeded") return <BookModal />;
  }

  return (
    <div className="library__container">
      <SearchForm />
      <div className="library__selectContainer">
        <ListSelection />
        <BooksPerPage />
      </div>
      {!favListLength && <BookAnimation text="Empty list" />}
      <BooksList />
      <Paging />
      {openModal && displayModal()}
    </div>
  );
}

export default LibraryFavoriteBooks;
