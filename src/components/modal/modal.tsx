import React, { useState, useEffect } from "react";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { clearModal } from "../../features/modalSlice";
import { useNavigate } from "react-router-dom";
import { addFavBook, removeFavBook } from "../../features/booksSlice";
import "./modal.css";

export default function BookModal() {
  const book = useAppSelector((state) => state.modal.book);
  const {
    id,
    title,
    authors,
    description,
    rating,
    genre_list,
    image_url,
    Quote1,
    Quote2,
    Quote3,
  } = book;
  const favBooksList = useAppSelector((state) => state.books.favBooks);
  const currentPageFav = useAppSelector(
    (state) => state.pageConfig.currentPageFav
  );
  const currentPage = useAppSelector((state) => state.pageConfig.currentPage);
  const booksPerPage = useAppSelector((state) => state.pageConfig.booksPerPage);
  const selectedList = useAppSelector((state) => state.books.selectedList);

  const [copyText, setCopyText] = useState("Copy");
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  function setGenres(genreList: string) {
    let genresList = genreList.split(",");
    return genresList.map((genre) => {
      return (
        <span key={genre} className="bookModal__genre">
          {genre}
        </span>
      );
    });
  }
  function setQuote(quote: string) {
    return (
      <div
        className="bookModal__quote"
        onMouseLeave={() => setCopyText("Copy")}
      >
        <p className="bookModal__quoteText">{quote}</p>
        <span className="bookModal__copyText">{copyText}</span>
        <ContentCopyIcon
          className="bookModal__copyIcon"
          onClick={() => {
            navigator.clipboard.writeText(quote);
            setCopyText("Copied");
          }}
        />
      </div>
    );
  }

  function closeModal(
    event:
      | React.MouseEvent<HTMLDivElement, MouseEvent>
      | React.MouseEvent<SVGSVGElement, MouseEvent>
      | KeyboardEvent
  ) {
    event.stopPropagation();
    dispatch(clearModal());
    selectedList === "all"
      ? navigate(`/per_page/${booksPerPage}/page/${currentPage}`, {
          replace: true,
        })
      : navigate(`/favBooks/per_page/${booksPerPage}/page/${currentPageFav}`);
  }

  const handleFavIconClick = () => {
    favBooksList.find((book) => book.id === id)
      ? dispatch(removeFavBook({ favBookId: id }))
      : dispatch(addFavBook({ favBook: book }));
  };

  useEffect(() => {
    const keyDownHandler = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        event.preventDefault();
        closeModal(event);
      }
    };

    document.addEventListener("keydown", keyDownHandler);
    return () => {
      document.removeEventListener("keydown", keyDownHandler);
    };
  });

  return (
    <div
      className="bookModal__background"
      onClick={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) =>
        closeModal(event)
      }
    >
      <div
        className="bookModal__container"
        onClick={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) =>
          event.stopPropagation()
        }
      >
        <CloseRoundedIcon
          className="bookModal__closeIcon"
          fontSize="large"
          sx={{ color: "#543a2b" }}
          onClick={(event) => {
            closeModal(event);
          }}
        />
        <img src={image_url} alt="Book cover" className="bookModal__img" />
        <div className="bookModal__detailsContainer">
          <div>
            <p className="bookModal__title">{title}</p>
            {favBooksList.find((book) => book.id === id) ? (
              <FavoriteIcon
                className="bookModal__heartIcon"
                onClick={() => {
                  handleFavIconClick();
                }}
              />
            ) : (
              <FavoriteBorderIcon
                className="bookModal__heartIcon"
                onClick={() => {
                  handleFavIconClick();
                }}
                sx={{
                  "&:hover": {
                    filter: "drop-shadow(0px 0px 2px #543a2b)",
                    transition: " all 0.3s ease-in-out",
                  },
                }}
              />
            )}
          </div>
          <p className="bookModal__authors">{authors}</p>
          <div className="bookModal__columns">
            <div className="bookModal__descriptionContainer">
              <span className="bookModal__ratingGenreList">
                <span className="bookModal__rating">{rating}</span>
                {setGenres(genre_list)}
              </span>
              <p className="bookModal__description">{description}</p>
            </div>
            <div className="bookModal__quotes">
              {setQuote(Quote1)}
              {setQuote(Quote2)}
              {setQuote(Quote3)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
