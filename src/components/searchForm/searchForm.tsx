import { useState, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { setSearchedBook } from "../../features/booksSlice";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import "./searchForm.css";

export default function SearchForm() {
  const dispatch = useAppDispatch();
  const searched = useAppSelector(
    (state) => state.books.searchedBook
  ) as string;
  const [searchedBook, setSearchBook] = useState<string>("");

  const updateReduxState = () => {
    dispatch(setSearchedBook({ searchedBook: searchedBook }));
  };

  const handleEnterPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") updateReduxState();
  };

  const handleCloseIconClick = () => {
    setSearchBook("");
    dispatch(setSearchedBook({ searchedBook: "" }));
  };

  useEffect(() => {
    setSearchBook(searched);
  }, [searched]);

  return (
    <div className="searchForm_container">
      <div className="searchForm_inputContainer">
        <input
          className="searchForm_input"
          type="text"
          value={searchedBook}
          placeholder="Search a book"
          onChange={(event) => {
            setSearchBook(event.target.value);
          }}
          onKeyDown={handleEnterPress}
        ></input>
        <CloseRoundedIcon
          className="searchForm_closeIcon"
          fontSize="large"
          sx={{ color: "#543a2b" }}
          onClick={handleCloseIconClick}
        />
      </div>
      <button className="searchForm_btn" onClick={updateReduxState}>
        Search
      </button>
    </div>
  );
}
