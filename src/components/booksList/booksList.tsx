import { useAppSelector } from "../../app/hooks";
import BookCard from "../bookCard/bookCard";
import "./booksList.css";

export default function BooksList() {
  let books = useAppSelector((state) => state.books.books);

  return (
    <div className="booksList__container">
      {books.map((book) => {
        return <BookCard key={book.id} book={book} />;
      })}
    </div>
  );
}
