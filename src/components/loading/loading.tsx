import React from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useNavigate } from "react-router-dom";
import { clearModal } from "../../features/modalSlice";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import BookAnimation from "../bookAnimation/bookAnimation";
import "./loading.css";

export default function Loading() {
  const currentPage = useAppSelector((state) => state.pageConfig.currentPage);
  const booksPerPage = useAppSelector((state) => state.pageConfig.booksPerPage);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  function closeModal(
    event:
      | React.MouseEvent<HTMLDivElement, MouseEvent>
      | React.MouseEvent<SVGSVGElement, MouseEvent>
  ) {
    event.stopPropagation();
    dispatch(clearModal());
    navigate(`/per_page/${booksPerPage}/page/${currentPage}`, {
      replace: true,
    });
  }

  return (
    <div
      className="loading__background"
      onClick={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) =>
        closeModal(event)
      }
    >
      <div
        className="loading__container"
        onClick={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) =>
          event.stopPropagation()
        }
      >
        <div className="loading__detailsContainer">
          <CloseRoundedIcon
            className="loading__closeIcon"
            fontSize="large"
            sx={{ color: "#543a2b" }}
            onClick={(event) => {
              closeModal(event);
            }}
          />
          <BookAnimation text="Loading..." />
        </div>
      </div>
    </div>
  );
}
