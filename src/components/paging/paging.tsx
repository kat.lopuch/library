import Pagination from "@mui/material/Pagination";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import {
  setCurrentPage,
  setCurrentPageFav,
} from "../../features/pageConfigSlice";
import useMediaQuery from "@mui/material/useMediaQuery";
import { createTheme, PaginationItem, ThemeProvider } from "@mui/material";
import "./paging.css";

const themeLarge = createTheme({
  components: {
    MuiPaginationItem: {
      styleOverrides: {
        root: {
          width: "2.7rem",
          height: "2.7rem",
          margin: "0.25rem",
          fontSize: "1.3rem",
          color: "#f1d9c6",
          fontSizeAdjust: "1.3em",
          borderColor: "#f1d9c6",
          borderRadius: "5px",
          transition: "all ease-in-out 0.5s",
          "&:hover": {
            backgroundColor: "#f1d9c6a1",
            color: "#543a2b",
            borderColor: "#f1d9c6",
          },
        },
        ellipsis: {
          color: "#f1d9c6",
          "&:hover": {
            backgroundColor: "transparent",
            color: "#f1d9c6",
            borderColor: "transparent",
          },
        },
        previousNext: {
          width: "2.5rem",
          height: "2.5rem",
          margin: "0.5rem",
        },
      },
    },
  },
});

const themeSmall = createTheme({
  components: {
    MuiPaginationItem: {
      styleOverrides: {
        root: {
          width: "1.6rem",
          height: "1.6rem",
          margin: "0.15rem",
          fontSize: "0/9rem",
          color: "#f1d9c6",
          borderColor: "#f1d9c6",
          borderRadius: "5px",
          transition: "all ease-in-out 0.5s",
          "&:hover": {
            backgroundColor: "#f1d9c6a1",
            color: "#543a2b",
            borderColor: "#f1d9c6",
          },
        },
        ellipsis: {
          color: "#f1d9c6",
          "&:hover": {
            backgroundColor: "transparent",
            color: "#f1d9c6",
            borderColor: "transparent",
          },
        },
        previousNext: {
          width: "1.4rem",
          height: "1.4rem",
          margin: "0.3rem",
        },
      },
    },
  },
});

export default function Paging() {
  const matches = useMediaQuery("(max-width:550px)");

  const selectedList = useAppSelector((state) => state.books.selectedList);
  const nrOfAllBooks = useAppSelector((state) => state.books.nrOfAllBooks);
  const nrOfFavBooks = useAppSelector((state) => state.books.favBooks.length);
  const booksPerPage = useAppSelector((state) => state.pageConfig.booksPerPage);
  const currentPage = useAppSelector((state) => state.pageConfig.currentPage);
  const currentPageFav = useAppSelector(
    (state) => state.pageConfig.currentPageFav
  );
  const totalPages = Math.ceil(nrOfAllBooks / booksPerPage);
  const totalPagesFav = Math.ceil(nrOfFavBooks / booksPerPage);
  const dispatch = useAppDispatch();

  const handleChange = (event: object, newPage: number) => {
    selectedList === "all"
      ? dispatch(setCurrentPage({ page: newPage }))
      : dispatch(setCurrentPageFav({ page: newPage }));
  };

  const searchBook = useAppSelector((state) => state.books.searchedBook);
  if (searchBook) return <span></span>;

  return (
    <div className="paging__container">
      <ThemeProvider theme={matches ? themeSmall : themeLarge}>
        <Pagination
          variant="outlined"
          count={selectedList === "all" ? totalPages : totalPagesFav}
          page={selectedList === "all" ? currentPage : currentPageFav}
          onChange={handleChange}
          size="small"
          renderItem={(item) => (
            <PaginationItem
              {...item}
              sx={{
                "&.Mui-selected": {
                  backgroundColor: "#f1d9c6",
                  color: "#543a2b",
                  borderColor: "#f1d9c6",
                },
                "&.Mui-selected:hover": {
                  backgroundColor: "#f1d9c6",
                  color: "#543a2b",
                  borderColor: "#f1d9c6",
                },
              }}
            />
          )}
        />
      </ThemeProvider>
    </div>
  );
}
