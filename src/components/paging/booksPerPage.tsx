import PerPageButton from "./perPageButton";
import { useAppSelector } from "../../app/hooks";
import "./booksPerPage.css";

export default function BooksPerPage() {
  const searchBook = useAppSelector((state) => state.books.searchedBook);
  if (searchBook) return <span></span>;
  return (
    <div className="booksPerPage__container">
      <PerPageButton perPageNumber={12} />
      <PerPageButton perPageNumber={24} />
      <PerPageButton perPageNumber={48} />
    </div>
  );
}
