import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  setCurrentPageFav,
  setPageAndBooksPerPage,
} from "../../features/pageConfigSlice";
import "./booksPerPage.css";

export default function PerPageButton(props: { perPageNumber: number }) {
  const currentBookPerPage = useAppSelector(
    (state) => state.pageConfig.booksPerPage
  );
  const dispatch = useAppDispatch();
  const changeBooksPerPage = (value: number) => {
    dispatch(setPageAndBooksPerPage({ page: 1, booksPerPage: value }));
    dispatch(setCurrentPageFav({ page: 1 }));
  };

  const selectedClass = (value: number, page: number) => {
    return value === page ? "booksPerPage__btn-selected" : "booksPerPage__btn";
  };

  return (
    <button
      className={selectedClass(props.perPageNumber, currentBookPerPage)}
      onClick={() => changeBooksPerPage(props.perPageNumber)}
    >
      {props.perPageNumber}
    </button>
  );
}
