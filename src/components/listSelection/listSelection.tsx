import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { setSelectedList } from "../../features/booksSlice";
import { useNavigate } from "react-router-dom";
import "./listSelection.css";

export default function ListSelection() {
  const currentList = useAppSelector((state) => state.books.selectedList);
  const booksPerPage = useAppSelector((state) => state.pageConfig.booksPerPage);
  const currentPage = useAppSelector((state) => state.pageConfig.currentPage);
  const currentPageFav = useAppSelector(
    (state) => state.pageConfig.currentPageFav
  );

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const selectedClass = (value: string) => {
    return value === currentList
      ? "listSelection__btn-selected"
      : "listSelection__btn";
  };
  return (
    <div className="listSelection__container">
      <button
        className={selectedClass("all")}
        onClick={() => {
          dispatch(setSelectedList({ list: "all" }));
          navigate(`/per_page/${booksPerPage}/page/${currentPage}`);
        }}
      >
        All
      </button>
      <button
        className={selectedClass("favorites")}
        onClick={() => {
          dispatch(setSelectedList({ list: "favorites" }));
          navigate(`/favBooks/per_page/${booksPerPage}/page/${currentPageFav}`);
        }}
      >
        Favorites
      </button>
    </div>
  );
}
