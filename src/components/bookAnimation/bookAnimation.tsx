import "./bookAnimation.css";

interface BookAnimationProps {
  text: string;
}

export default function BookAnimation(props: BookAnimationProps) {
  return (
    <div className="bookAnimation__container">
      <div className="bookAnimation__bookContainer">
        <div className="bookAnimation__book">
          <div className="bookAnimation__inner">
            <div className="bookAnimation__left"></div>
            <div className="bookAnimation__middle"></div>
            <div className="bookAnimation__right"></div>
          </div>
          <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
        <p className="bookAnimation__text">{props.text}</p>
      </div>
    </div>
  );
}
