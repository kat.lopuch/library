import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { setBookId, setOpenModal } from "../../features/modalSlice";
import { addFavBook, Book, removeFavBook } from "../../features/booksSlice";
import { useNavigate } from "react-router-dom";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import "./bookCard.css";

export default function BookCard(props: { book: Book }) {
  const favBooksList = useAppSelector((state) => state.books.favBooks);
  const { id, title, authors, description, rating, genre_list, image_url } =
    props.book;
  const selectedList = useAppSelector((state) => state.books.selectedList);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  function setGenres(genreList: string) {
    let genresList = genreList.split(",");
    return genresList.map((genre) => {
      return (
        <span key={genre} className="bookCard__genre">
          {genre}
        </span>
      );
    });
  }
  const handleFavIconClick = () => {
    favBooksList.find((book) => book.id === id)
      ? dispatch(removeFavBook({ favBookId: id }))
      : dispatch(addFavBook({ favBook: props.book }));
  };

  const onOpenHandler = () => {
    dispatch(setBookId({ bookId: id }));
    dispatch(setOpenModal({ openModal: true }));
    selectedList === "all" ? navigate(`/id/${id}`) : navigate(`/favId/${id}`);
  };

  return (
    <div className="bookCard__container">
      <div className="bookCard__imgContainer" onClick={onOpenHandler}>
        <div className="bookCard__img-background"></div>
        <img src={image_url} alt="Book cover" className="bookCard__img" />
      </div>
      {favBooksList.find((book) => book.id === id) ? (
        <FavoriteIcon
          className="bookCard__heartIcon"
          onClick={handleFavIconClick}
        />
      ) : (
        <FavoriteBorderIcon
          className="bookCard__heartIcon"
          onClick={handleFavIconClick}
          sx={{
            "&:hover": {
              filter: "drop-shadow(0px 0px 2px #543a2b)",
              transition: " all 0.3s ease-in-out",
            },
          }}
        />
      )}

      <div className="bookCard__detailsContainer" onClick={onOpenHandler}>
        <div className="bookCard__firstLine">
          <span className="bookCard__title">{title}</span>
          <span className="bookCard__rating">{rating}</span>
        </div>
        <p className="bookCard__authors">{authors}</p>
        <p className="bookCard__genreList">{setGenres(genre_list)}</p>
        <p className="bookCard__description">{description}</p>
      </div>
      <button className="bookCard__btn" onClick={onOpenHandler}>
        Open
      </button>
    </div>
  );
}
