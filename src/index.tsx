import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./app/store";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LibraryAllBooks from "./LibraryAllBooks";
import LibraryFavoriteBooks from "./LibraryFavoriteBooks";
import "./index.css";

const container = document.getElementById("root")!;
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <Routes>
          <Route path="/" element={<LibraryAllBooks />}></Route>
          <Route path="/search/:search" element={<LibraryAllBooks />}></Route>
          <Route path="/id/:id" element={<LibraryAllBooks />}></Route>
          <Route path="/favId/:id" element={<LibraryFavoriteBooks />}></Route>
          <Route path="/favBooks" element={<LibraryFavoriteBooks />}></Route>
          <Route
            path="/favBooks/per_page/:per_page/page/:page"
            element={<LibraryFavoriteBooks />}
          ></Route>
          <Route
            path="/favBooks/search/:search"
            element={<LibraryFavoriteBooks />}
          ></Route>
          <Route
            path="/per_page/:per_page/page/:page"
            element={<LibraryAllBooks />}
          ></Route>
        </Routes>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);
